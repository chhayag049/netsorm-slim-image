/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: netstorm
    Date of recording: 05/06/2017 04:38:48
    Flow details:
    Build details: 4.1.8 (build# 1503)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
	
    ns_start_transaction("Home");
    ns_web_url ("Home",
     //"URL=http://127.0.0.1/tours/index.html",
        "URL=http://127.0.0.1/tours/index.html",
      //  "URL=http://127.0.0.1/tours/indx.html",
        "HEADER=Accept-Language:en-in",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1494068768035.png",
        "Snapshot=webpage_1494068782747.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("Home", NS_AUTO_STATUS);
    ns_page_think_time(15.565);

    ns_start_transaction("Login");
    ns_web_url ("Login",
    // "URL=http://127.0.0.1/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=kirti&password=bansal&login.x=36&login.y=15&login=Login&JSFormSubmit=off",
        "URL=http://127.0.0.1/cgi-bin/login?userSession={SessionIdSP}&username={unameFP}&password={upassFP}&login.x=36&login.y=15&login=Login&JSFormSubmit=off",
   //     "URL=http://127.0.0.1/cgi-bin/logn?userSession={SessionIdSP}&username={UsernameFP}&password={UserpassFP}&login.x=36&login.y=15&login=Login&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068795047.png",
        "Snapshot=webpage_1494068798361.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("Login", NS_AUTO_STATUS);
    ns_page_think_time(2.239);
    

    ns_start_transaction("Reservation");
    ns_web_url ("Reservation",
         //"URL=http://127.0.0.1/cgi-bin/reservation",
        "URL=http://127.0.0.1/cgi-bin/reservation",
     //   "URL=http://127.0.0.1/cgi-bn/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068800213.png",
        "Snapshot=webpage_1494068804197.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("Reservation", NS_AUTO_STATUS);
    ns_page_think_time(13.775);

    ns_start_transaction("FindFlight");
    ns_web_url ("FindFlight",
    // "URL=http://127.0.0.1/cgi-bin/findflight?depart={DeparCity}&departDate=05-07-2017&arrive={ArrivalCity}&returnDate=05-08-2017&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=32&findFlights.y=8&findFlights=Submit",
      "URL=http://127.0.0.1/cgi-bin/findflight?depart={DepartcityFP}&departDate={Departdate}&arrive={ArrivalcityFP}&returnDate={Arrivaldate}&numPassengers={NumOfPsgRN}&seatPref={SeatprefFP}&seatType={SeattypeFP}&findFlights.x=32&findFlights.y=8&findFlights=Submit",
   //    "URL=http://127.0.0.1/cgi-bn/findflight?depart={DepartcityFP}&departDate={Departdate}&arrive={ArrivalcityFP}&returnDate={Arrivaldate}&numPassengers={NumOfPsgRN}&seatPref={SeatprefFP}&seatType={SeattypeFP}&findFlights.x=32&findFlights.y=8&findFlights=Submit",
          "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068817750.png",
        "Snapshot=webpage_1494068822702.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("FindFlight", NS_AUTO_STATUS);
    ns_page_think_time(3.059);

    ns_start_transaction("FlightDetails");
    ns_web_url ("FlightDetails",
        // "URL=http://127.0.0.1/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-07-2017&hidden_outboundFlight_button1=001%7C0%7C05-07-2017&hidden_outboundFlight_button2=002%7C0%7C05-07-2017&hidden_outboundFlight_button3=003%7C0%7C05-07-2017&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=45&reserveFlights.y=7",
         "URL=http://127.0.0.1/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-07-2017&hidden_outboundFlight_button1=001%7C0%7C05-07-2017&hidden_outboundFlight_button2=002%7C0%7C05-07-2017&hidden_outboundFlight_button3=003%7C0%7C05-07-2017&numPassengers={NumOfPsgRN}&advanceDiscount=&seatType={SeattypeFP}&seatPref={SeatprefFP}&reserveFlights.x=45&reserveFlights.y=7",
      //  "URL=http://127.0.0.1/cgi-bin/findfliht?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-07-2017&hidden_outboundFlight_button1=001%7C0%7C05-07-2017&hidden_outboundFlight_button2=002%7C0%7C05-07-2017&hidden_outboundFlight_button3=003%7C0%7C05-07-2017&numPassengers={NumOfPsgRN}&advanceDiscount=&seatType={SeattypeFP}&seatPref={SeatprefFP}&reserveFlights.x=45&reserveFlights.y=7",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068825762.png",
        "Snapshot=webpage_1494068829902.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("FlightDetails", NS_AUTO_STATUS);
    ns_page_think_time(81.766);

    ns_start_transaction("FlightConfirmation");
    ns_web_url ("FlightConfirmation",
         // "URL=http://127.0.0.1/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=12345789923&expDate=04-20&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C05-07-2017&advanceDiscount=&buyFlights.x=69&buyFlights.y=15&.cgifields=saveCC",
        "URL=http://127.0.0.1/cgi-bin/findflight?firstName={FirstnameFP}&lastName={LastnameFP}&address1={Address1FP}&address2={Address2FP}&pass1={PassengerNameFP}&creditCard={CreditCardUN}&expDate={Expirydate}&oldCCOption=&numPassengers={NumOfPsgRN}&seatType={SeattypeFP}&seatPref={SeatprefFP}&outboundFlight=000%7C0%7C05-07-2017&advanceDiscount=&buyFlights.x=69&buyFlights.y=15&.cgifields=saveCC",
     //   "URL=http://127.0.0.1/cgi-bin/fndflight?firstName={FirstnameFP}&lastName={LastnameFP}&address1={Address1FP}&address2={Address2FP}&pass1={PassengerNameFP}&creditCard={CreditCardUN}&expDate={Expirydate}&oldCCOption=&numPassengers={NumOfPsgRN}&seatType={SeattypeFP}&seatPref={SeatprefFP}&outboundFlight=000%7C0%7C05-07-2017&advanceDiscount=&buyFlights.x=69&buyFlights.y=15&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068911123.png",
        "Snapshot=webpage_1494068915116.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/bookanother.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("FlightConfirmation", NS_AUTO_STATUS);
    ns_page_think_time(3.087);

    ns_start_transaction("Logout");
    ns_web_url ("Logout",
        //"URL=http://127.0.0.1/cgi-bin/welcome",
        "URL=http://127.0.0.1/cgi-bin/welcome",
      // "URL=http://127.0.0.1/cgi-bin/wlcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1494068917587.png",
        "Snapshot=webpage_1494068920382.png",
        INLINE_URLS,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://127.0.0.1/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("Logout", NS_AUTO_STATUS);
    ns_page_think_time(4.745);

}

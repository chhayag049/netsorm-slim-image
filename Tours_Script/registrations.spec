//Checkpoints in Scripts
nsl_web_find(TEXT="Mercury Tours", PAGE=Home, FAIL=NOTFOUND, ID="Page fails due to string not match.", ActionOnFail=STOP);
nsl_web_find(TEXT="Welcome to Mercury Tours", PAGE=Login, FAIL=NOTFOUND, ID="Page fails due to string not match", ActionOnFail=STOP);
nsl_web_find(TEXT="Find Flight", PAGE=Reservation, FAIL=NOTFOUND, ID="Page fails due to string not match", ActionOnFail=STOP);
nsl_web_find(TEXT="Search Results", PAGE=FindFlight, FAIL=NOTFOUND, ID="Page fails due to string not match", ActionOnFail=STOP);
nsl_web_find(TEXT="Method of Payment", PAGE=FlightDetails, FAIL=NOTFOUND, ID="Page fails due to string not match", ActionOnFail=STOP);
nsl_web_find(TEXT="Flight Confirmation", PAGE=FlightConfirmation, FAIL=NOTFOUND, ID="Page fails due to string not match.", ActionOnFail=STOP);
nsl_web_find(TEXT="Mercury Tours", PAGE=Logout, FAIL=NOTFOUND, ID="Page fails due to string match", ActionOnFail=STOP);

//Login User name,Password and SessionID in Login Page 
nsl_static_var(UsernameFP:1,UserpassFP:2, File=UserDetails.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_search_var(SessionIdSP, PAGE=Home, LB="userSession value", RB=">", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//Depart & Arrival parameters in Find Flight
nsl_static_var(DepartcityFP:1,ArrivalcityFP:2, File=CitySelection.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_date_var(Departdate, Format="%m/%d/%Y", Unique=YES, Offset=1.00:00:00, Refresh=SESSION);
nsl_date_var(Arrivaldate, Format="%m/%d/%Y", Unique=YES, Offset=5.00:00:00, Refresh=SESSION);

//NumofPassengers and Seat Details in Find Flight,Flight Details Page and Flight Confirmation Page  
nsl_random_number_var(NumOfPsgRN, Min=4, Max=6, Format=%01lu, Refresh=SESSION);
nsl_static_var(SeatprefFP:1,SeattypeFP:2, File=SeatDetails.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);

//PassengerInformation Parameters in Flight Confirmation Page
nsl_static_var(FirstnameFP:1,LastnameFP:2,Address1FP:3,Address2FP:4,PassengerNameFP:5, File=Carddetails.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_unique_number_var(CreditCardUN, Format=%01lu, Refresh=SESSION);
nsl_date_var(Expirydate, Format="%m/%d/%Y", Offset=1800.00:00:00, Refresh=SESSION);
nsl_static_var(unameFP:1,upassFP:2, File=LoginData.seq, Refresh=USE, Mode=SEQUENTIAL, FirstDataLine=2, EncodeMode=All);
